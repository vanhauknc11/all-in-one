import Container from "@/app/components/Container";
import React from "react";
import AbsContentItem, { AbsContentProps } from "./AbsContentItem";

const dataAbs: AbsContentProps[] = [
  {
    title: "Using Abstract",
    description:
      "Abstract lets you manage, version, and document your designs in one place.",
    imgName: "item1.png",
  },
  {
    title: "Manage your account",
    description:
      "Configure your account settings, such as your email, profile details, and password.",
    imgName: "item2.png",
  },
  {
    title: "Manage organizations, teams, and projects",
    description:
      "Use Abstract organizations, teams, and projects to organize your people and your work.",
    imgName: "item3.png",
  },
  {
    title: "Manage billing",
    description: "Change subscriptions and payment details.",
    imgName: "item4.png",
  },
  {
    title: "Authenticate to Abstract",
    description:
      "Set up and configure SSO, SCIM, and Just-in-Time provisioning.",
    imgName: "item5.png",
  },
  {
    title: "Abstract support",
    description: "Get in touch with a human.",
    imgName: "item6.png",
  },
];

const AbsContent = () => {
  return (
    <div className="bg-white py-20">
      <Container>
        <div className="grid grid-cols-1 md:grid-cols-2 gap-5 content-center md:mx-20">
          {dataAbs.map((data, index) => (
            <AbsContentItem
              key={index}
              title={data.title}
              description={data.description}
              imgName={data.imgName}
            />
          ))}
        </div>
      </Container>
    </div>
  );
};

export default AbsContent;
