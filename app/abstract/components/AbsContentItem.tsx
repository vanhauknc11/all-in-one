import Image from "next/image";
import React from "react";
import { AiOutlineArrowRight } from "react-icons/ai";

export interface AbsContentProps {
  title: string;
  description: string;
  imgName: string;
}

const AbsContentItem: React.FC<AbsContentProps> = ({
  title,
  description,
  imgName,
}) => {
  return (
    <div className="flex items-start gap-5 my-4">
      <Image
        src={`/images/abs/${imgName}`}
        alt="item1"
        width={100}
        height={100}
      />
      <div className="content flex flex-col gap-3 max-w-[400px]">
        <h2 className="font-semibold text-2xl">{title}</h2>
        <p>{description}</p>
        <a href="#" className="text-blue-500 flex gap-1 items-center">
          <span>Learn more</span> <AiOutlineArrowRight />
        </a>
      </div>
    </div>
  );
};

export default AbsContentItem;
