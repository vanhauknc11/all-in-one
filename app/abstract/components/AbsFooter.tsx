import Container from "@/app/components/Container";
import React from "react";
import FooterItems, { FooterItemProps } from "./FooterItems";

const listItems: FooterItemProps[] = [
  { title: "Abstract", items: ["Start Trial", "Pricing", "Download"] },
  {
    title: "Resources",
    items: ["Blog", "Help center", "Release Notes", "Status"],
  },
  { title: "Community", items: ["Twitter", "LinkedIn", "Facebook", "Podcast"] },
  { title: "Company", items: ["About Us", "Carrers", "Legal"] },
];

const AbsFooter = () => {
  return (
    <div className="bg-black text-white py-10">
      <Container>
        <div className="grid grid-cols-2 md:grid-cols-4 gap-5 px-5">
          {listItems.map((data, index) => (
            <FooterItems key={index} title={data.title} items={data.items} />
          ))}
        </div>
      </Container>
    </div>
  );
};

export default AbsFooter;
