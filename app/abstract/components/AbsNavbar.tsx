"use client";
import Container from "@/app/components/Container";
import Image from "next/image";
import { BiSearch } from "react-icons/bi";
import { IoClose, IoMenu } from "react-icons/io5";
import React from "react";

const AbsNavbar = () => {
  const [showMenu, setShowMenu] = React.useState(false);
  return (
    <div className="bg-black text-white w-full">
      <Container>
        <div className="flex justify-between items-center py-6">
          <div className="flex items-center gap-5">
            <Image
              className="cursor-pointer"
              src="/images/abs/logo.svg"
              alt="logo"
              width={130}
              height={130}
            />

            <span className="text-logo relative text-xl cursor-pointer hover:underline">
              Help Center
            </span>
          </div>
          <div className="flex items-center gap-1">
            <div className="hidden md:block bg-[#191a1b] py-2 px-7 text-xl text-white rounded-md border-[1px] border-white cursor-pointer">
              Submit a request
            </div>
            <div className="ml-5 hidden md:block bg-[#4C5FD5] py-2 px-7 text-xl text-white rounded-md border-transparent cursor-pointer hover:bg-white hover:text-black transition">
              Sign in
            </div>

            <BiSearch className="md:hidden" size={32} />
            {showMenu ? (
              <IoClose
                className="md:hidden"
                size={32}
                onClick={() => {
                  setShowMenu(!showMenu);
                }}
              />
            ) : (
              <IoMenu
                className="md:hidden"
                size={32}
                onClick={() => {
                  setShowMenu(!showMenu);
                }}
              />
            )}
          </div>
        </div>
      </Container>
      {showMenu && (
        <div
          className={`md:hidden relative bottom-0 bg-black w-full border-t-gray-500 border-t-[1px] translate duration-300`}
        >
          <div className="flex flex-col items-center py-5 ">
            <div className="py-6 text-xl border-b-gray-500 border-b-[1px]">
              Submit a request
            </div>
            <div className="py-6 text-xl">Sign in</div>
          </div>
        </div>
      )}
    </div>
  );
};

export default AbsNavbar;
