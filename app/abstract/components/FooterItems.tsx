import React from "react";

export interface FooterItemProps {
  title: string;
  items: string[];
}

const FooterItems: React.FC<FooterItemProps> = ({ title, items }) => {
  return (
    <div className="info">
      <h2 className="text-2xl font-semibold mb-3">{title}</h2>
      <ul>
        {items.map((item, key) => (
          <li key={key} className="cursor-pointer hover:underline">
            {item}
          </li>
        ))}
      </ul>
    </div>
  );
};

export default FooterItems;
