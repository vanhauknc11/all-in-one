import React from "react";
import { AiOutlineArrowRight } from "react-icons/ai";

const SearchComponent = () => {
  return (
    <div className="mx-auto bg-[#DADBF1] py-40 flex flex-col items-center">
      <h2 className="text-xl md:text-[60px] mb-7">How can we help?</h2>

      <div className="w-3/4 md:w-1/2 relative">
        <AiOutlineArrowRight
          className="absolute right-2 top-4 text-neutral-700"
          size={32}
        />
        <input className="w-full py-5 rounded-md border-[1px] px-4 font-light outline-none border-neutral-300 focus:border-blue-500" />
      </div>
    </div>
  );
};

export default SearchComponent;
