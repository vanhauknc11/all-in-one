import React from "react";
import AbsNavbar from "./components/AbsNavbar";
import "./abstract.css";
import SearchComponent from "./components/SearchComponent";
import AbsContent from "./components/AbsContent";
import AbsFooter from "./components/AbsFooter";
const AbstractPage = () => {
  return (
    <div>
      <AbsNavbar />
      <SearchComponent />
      <AbsContent />
      <AbsFooter />
    </div>
  );
};

export default AbstractPage;
