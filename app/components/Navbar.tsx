"use client";
import React from "react";
import Container from "./Container";
import Image from "next/image";
import Search from "./Search";
import { AiOutlineMenu } from "react-icons/ai";

const Navbar = () => {
  const [showMenu, setShowMenu] = React.useState(false);
  return (
    <div className="fixed bg-white w-full z-10 shadow-sm">
      <div className="py-4 border-b-[1px]">
        <Container>
          <div className="flex flex-row justify-between items-center gap-3 md:gap-0">
            <Image src="/images/logo.png" width={100} height={100} alt="Logo" />
            <Search />
            <div className="relative">
              <div className="flex items-center gap-3">
                <div className="hidden md:block text-sm font-semibold py-3 px-4 rounded-full hover:bg-neutral-100 transition cursor-pointer">
                  Airbnb your home
                </div>
                <div
                  className="flex items-center p-4 md:py-1 md:px-2 border-[1px] border-neutral-200 rounded-full gap-3 cursor-pointer hover:shadow transition"
                  onClick={() => {
                    setShowMenu(!showMenu);
                  }}
                >
                  <AiOutlineMenu />
                  <Image
                    className="rounded-full hidden md:block"
                    alt="avatar"
                    src="/images/placeholder.jpg"
                    width={30}
                    height={30}
                  />
                </div>
              </div>

              {showMenu && (
                <div className="absolute z-10 right-0 top-12 rounded-xl shadow-md w-[40vw] md:w-3/4 bg-white overflow-hidden text-sm">
                  <div className="flex flex-col cursor-pointer">
                    <div className="px-4 py-3 hover:bg-neutral-100 transition font-semibold">
                      My trips
                    </div>
                    <div className="px-4 py-3 hover:bg-neutral-100 transition font-semibold">
                      My favorites
                    </div>
                  </div>
                </div>
              )}
            </div>
          </div>
        </Container>
      </div>
    </div>
  );
};

export default Navbar;
