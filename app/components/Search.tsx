import React from "react";
import { BiSearch } from "react-icons/bi";

const Search = () => {
  return (
    <div className="bg-white border-[1px] w-full md:w-auto rounded-full py-2 shadow-sm hover:shadow-md transition cursor-pointer">
      <div className="flex justify-between items-center">
        <div className="text-sm font-semibold px-6">Anywhere</div>
        <div className="hidden md:block border-x-[1px] flex-1 text-center  text-sm font-semibold px-6">
          Any Week
        </div>
        <div className="text-sm font-semibold pl-6 pr-2 text-gray-600 flex items-center gap-3">
          <div className="hidden md:block">Add Guests</div>
          <div className="p-2 bg-rose-500 rounded-full text-white">
            <BiSearch size={18} />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Search;
