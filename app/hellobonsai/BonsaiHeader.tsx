import React from "react";
import Container from "../components/Container";
import Image from "next/image";

import BonsaiSubmenu, {
  NoMenuItem,
  BonsaiSubMenuSecond,
  BonsaiSubMenuThird,
  BonsaiSubMenuFour,
} from "./Submenu/BonsaiSubmenu";
interface MenuType {
  name: string;
  subMenu: React.ReactNode | null;
}

const BonsaiHeader = () => {
  const menus: MenuType[] = [
    { name: "Product", subMenu: <BonsaiSubmenu /> },
    { name: "Industries", subMenu: <BonsaiSubMenuSecond /> },
    { name: "Templates", subMenu: <BonsaiSubMenuThird /> },
    {
      name: "Pricing",
      subMenu: <NoMenuItem />,
    },
    { name: "Resources", subMenu: <BonsaiSubMenuFour /> },
    {
      name: "Reviews",
      subMenu: <NoMenuItem />,
    },
  ];
  return (
    <div className="bg-white w-full relative hidden md:block">
      <Container>
        <div className="flex flex-row justify-between items-center w-full  border-b-[1px] border-gray-300">
          <div className="flex ">
            <Image
              src="/images/bonsai/logo.svg"
              alt="logo"
              width={100}
              height={100}
            />
            <ul className="flex justify-between items-center  ml-12 text-gray-500">
              {menus.map((menu, index) => (
                <li
                  key={index}
                  className="menu-item cursor-pointer py-10 px-2 font-medium hover:text-[#22ad01]"
                >
                  <span>{menu.name}</span>
                  <div className="absolute z-10 top-[90px] left-0 bg-white shadow-md hidden min-h-[300px] w-full">
                    <Container>{menu.subMenu && menu.subMenu}</Container>
                  </div>
                </li>
              ))}
            </ul>
          </div>
          <div className="flex items-center gap-5">
            <div className="cursor-pointer">Login</div>
            <div className="py-2 px-6 rounded-full bg-[#22ad01] text-white cursor-pointer">
              Start free
            </div>
          </div>
        </div>
        <div className="menu-child">
          <ul className="flex gap-7 py-2 text-gray-400">
            <li className="cursor-pointer hover:text-gray-500">
              Small businesses
            </li>
            <li className="cursor-pointer hover:text-gray-500">Agencies</li>
            <li className="cursor-pointer hover:text-gray-500">Freelancers</li>
          </ul>
        </div>
      </Container>
    </div>
  );
};

export default BonsaiHeader;
