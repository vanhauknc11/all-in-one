"use client";
import React from "react";
import Container from "../components/Container";
import Image from "next/image";
import { IoMenu } from "react-icons/io5";
import { AiOutlineArrowDown } from "react-icons/ai";

const BonsaiMobileHeader = () => {
  const [showMenu, setShowMenu] = React.useState(false);
  return (
    <div className="md:hidden bg-white relative">
      <Container>
        <div className="flex items-center justify-between py-4">
          <Image
            src="/images/bonsai/logo.svg"
            alt="logo"
            width={100}
            height={100}
          />

          <IoMenu
            size={40}
            className="p-2 rounded-full bg-green-950 text-white"
            onClick={() => {
              setShowMenu(!showMenu);
            }}
          />
        </div>
      </Container>

      <div
        className={`dropdown-menu absolute shadow-md w-full h-[600px] z-10 ${
          showMenu ? "open" : "close"
        }`}
      >
        <div className="dropdown-sub font-semibold ">
          <div className="border-b-[1px] border-gray-400 flex items-center justify-between">
            <span>Product</span>
            <AiOutlineArrowDown size={22} className="text-[#22ad01]" />
          </div>
          <div className="border-b-[1px] border-gray-400 flex items-center justify-between">
            <span>Industries</span>
            <AiOutlineArrowDown size={22} className="text-[#22ad01]" />
          </div>
          <div className="border-b-[1px] border-gray-400 flex items-center justify-between">
            <span>Templates</span>
            <AiOutlineArrowDown size={22} className="text-[#22ad01]" />
          </div>
          <div className="border-b-[1px] border-gray-400 flex items-center justify-between">
            <span>Pricing</span>
          </div>
          <div className="border-b-[1px] border-gray-400 flex items-center justify-between">
            <span>Resources</span>
            <AiOutlineArrowDown size={22} className="text-[#22ad01]" />
          </div>
          <div className="">
            <span>Review</span>
          </div>
          <div className="">
            <div className="rounded-full border-[2px] border-black text-center">
              Login
            </div>
            <div
              className="rounded-full border-[2px] 
            mt-4
            border-transparent text-center
             text-white bg-[#22ad01]"
            >
              Start free
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default BonsaiMobileHeader;
