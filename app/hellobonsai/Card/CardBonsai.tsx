import React from "react";

interface CardBonsaiProp {
  title: string;
  description: string;
  footerText: string;
}
const CardBonsai: React.FC<CardBonsaiProp> = ({
  title,
  description,
  footerText,
}) => {
  return (
    <div className="relative text-[#003c2f] w-[320px] min-h-[300px] rounded-xl border-[1px] border-[#d3dcd4] p-8">
      <h2 className="font-semibold text-lg my-4">{title}</h2>
      <p className="text-sm">{description}</p>
      <h2 className="text-[30px] font-bold absolute bottom-4 right-8">
        {footerText}
      </h2>
    </div>
  );
};

export default CardBonsai;
