import React from "react";
import CardBonsai from "./Card/CardBonsai";
import TitleBonsai from "./TitleBonsai";
import Container from "../components/Container";

const desOne =
  "Invite other users to specific projects for limited access and functionality.";
const desTwo =
  "Invite other users for full account access and company management.";
const desThree =
  "Track expenses, identify write-offs, and estimate quarterly taxes easily.";
const CardList = () => {
  return (
    <div className="flex flex-col items-center mb-7 ">
      <TitleBonsai title="Take it to the next level with add-ons" />
      <Container>
        <div className="my-5 grid grid-cols-1 md:grid-cols-3 gap-8">
          <CardBonsai
            title="Collaborators"
            description={desOne}
            footerText="Free"
          />
          <CardBonsai title="Partners" description={desTwo} footerText="$9" />
          <CardBonsai
            title="Tax Assistant"
            description={desThree}
            footerText="$10"
          />
        </div>
      </Container>
    </div>
  );
};

export default CardList;
