import React from "react";

interface FeedbackItemProp {
  content: string;
}
const FeedbackItem: React.FC<FeedbackItemProp> = ({ content }) => {
  return (
    <div className="bg-[#ecf1ec] p-5 text-lg text-black rounded-lg text-center font-semibold">
      {`“${content}”`}
    </div>
  );
};

export default FeedbackItem;
