import React from "react";
import FeedbackItem from "./Feeback/FeedbackItem";

const feedbacks = [
  "My best friend in scaling my business",
  "Like putting my finances on auto-pilot",
  "Bonsai does the hard work",
  "Everything is streamlined",
  "Huge timesaver",
  "It’s been the most stress-free year of my life!",
  "I feel more confident",
  "Backbone of my business",
  "So simple",
  "Clients love how easy my systems are",
  "A must-have!",
  "I do less admin and do more of what I love",
  "Worry-free contracts and invoices",
  "It pays for itself",
  "Great customer service!",
  "A life-saver!",
  "Clients take me more seriously",
  "I upped my rates and won more clients",
];

const FeedbackContent = () => {
  return (
    <div className="flex flex-row flex-wrap gap-5">
      {feedbacks.map((feedback, index) => (
        <FeedbackItem key={index} content={feedback} />
      ))}
    </div>
  );
};

export default FeedbackContent;
