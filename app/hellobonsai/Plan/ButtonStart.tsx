import React from "react";

const ButtonStart = () => {
  return (
    <div className="px-5 py-2 bg-[#22ad01] hover:bg-[#2bb90a] cursor-pointer text-white font-semibold text-center max-w-[150px] rounded-full">
      <span>Start for free</span>
    </div>
  );
};

export default ButtonStart;
