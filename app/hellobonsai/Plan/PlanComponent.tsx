import React from "react";
import ButtonStart from "./ButtonStart";
import { AiOutlineCheck } from "react-icons/ai";

interface PlanComponentProp {
  title: string;
  price: number;
  description: string;
  features: string[];
}

const PlanComponent: React.FC<PlanComponentProp> = ({
  title,
  price,
  description,
  features,
}) => {
  return (
    <div className="plan-shadow bg-white rounded-lg  max-w-sm p-6 flex flex-col items-center justify-center gap-2">
      <h2 className="font-bold text-[32px] text-[#003c2f] mt-4">{title}</h2>
      <div className="font-bold text-[32px] text-[#003c2f]">
        $ <span>{price}</span>{" "}
        <span className="text-sm text-[#65816d]">/month</span>
      </div>
      <ButtonStart />
      <p className="text-sm text-[#65816d] my-3 text-center">{description}</p>

      <ul className="plan-menu py-7 relative flex flex-col gap-3 ">
        {features.map((feature, index) => (
          <li className="flex gap-3 items-center" key={index}>
            <AiOutlineCheck size={22} className="text-[#22ad01]" />
            <span>{feature}</span>
          </li>
        ))}
      </ul>
      <ButtonStart />
    </div>
  );
};

export default PlanComponent;
