"use client";
import React from "react";
import useCheckMobileScreen from "../hooks/useCheckMobile";
import PlanComponent from "./Plan/PlanComponent";

const description =
  "Ideal for freelancers and contractors just starting out. Billed annually.";
const features = [
  "All Templates",
  "Unlimited Clients & Projects",
  "Invoicing & Payments",
  "Invoicing & Payments",
  "Scheduling",
  "Tasks & Time Tracking",
  "Client CRM",
  "Forms & Questionnaires",
  "Expense Tracking",
  "Up to 5 Project Collaborators",
];
const desProfessional =
  "Everything a growing independent business needs to thrive. Billed annually.";
const featuresProfessional = [
  "Everything in Starter, plus:",
  "Custom Branding",
  "Workflow Automations",
  "Client Portal",
  "Unlimited Scheduling events",
  "QuickBooks Integration",
  "Calendly Integration",
  "Zapier Integration",
  "Up to 15 Project Collaborators",
];

const desBusiness =
  "The perfect package for small businesses and agencies. Billed annually.";
const featureBusiness = [
  "Everything in Professional, plus:",
  "Subcontractor Management",
  "Hiring Agreement Templates",
  "Subcontractor Onboarding",
  "Talent Pool",
  "3 Team Seats",
  "Accountant Access",
  "Connect Multiple Bank Accounts",
  "Unlimited Subcontractors",
  "Unlimited Project Collaborators",
];

const PlanContent = () => {
  const [isChecked, setIsChecked] = React.useState(false);
  const isMobile = useCheckMobileScreen();

  return (
    <div className="mt-10 p-10 md:px-52">
      <h2 className="text-center text-[25px] md:text-[70px] text-[#003c2f] font-bold px-1  md:px-20 md:leading-[70px]">
        Pick the best plan for your business
      </h2>
      <div className="flex justify-center items-center rounded-full bg-[#f0f5f0] max-w-[400px] mx-auto my-10">
        <span className="switch border-inherit">
          <input
            type="checkbox"
            id="switcher"
            onChange={(e) => {
              setIsChecked(e.target.checked);
            }}
          />
          <label
            className={`${
              isMobile ? `switcher-label-mobile` : `switcher-label`
            }  `}
            htmlFor="switcher"
          ></label>
          <label className="absolute z-5 left-2 md:left-10 top-5 pointer-events-none wipe-one">
            Monthly
          </label>
          <label className="absolute z-5 right-2 md:right-7 top-5 pointer-events-none wipe-two">
            Yearly{" "}
            <span className="p-1 ml-1 font-semibold rounded-full text-sm bg-white text-[#22ad01]">
              2 months for free
            </span>
          </label>
        </span>
      </div>
      {isChecked ? (
        <div className="my-[100px] grid grid-cols-1 md:grid-cols-3 gap-8">
          <PlanComponent
            title="Starter"
            price={17}
            description={description}
            features={features}
          />
          <div className="bg-orange-400 px-1 pt-3 pb-0 rounded-2xl special-plan">
            <p className="text-center text-white text-sm pb-2">
              Best value for money
            </p>
            <PlanComponent
              title="Professional"
              price={32}
              description={desProfessional}
              features={featuresProfessional}
            />
          </div>
          <PlanComponent
            title="Business"
            price={52}
            description={desBusiness}
            features={featureBusiness}
          />
        </div>
      ) : (
        <div className="my-[100px] grid grid-cols-1 md:grid-cols-3 gap-8">
          <PlanComponent
            title="Starter"
            price={17}
            description={description}
            features={features}
          />

          <PlanComponent
            title="Starter"
            price={17}
            description={description}
            features={features}
          />
          <PlanComponent
            title="Starter"
            price={17}
            description={description}
            features={features}
          />
        </div>
      )}
    </div>
  );
};

export default PlanContent;
