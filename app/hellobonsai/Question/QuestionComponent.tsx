"use client";
import React from "react";
import { BsChevronDown, BsChevronUp } from "react-icons/bs";

interface QuestionComponentProp {
  title: string;
  description: string;
}

const QuestionComponent: React.FC<QuestionComponentProp> = ({
  title,
  description,
}) => {
  const [isOpen, setIsOpen] = React.useState(false);
  return (
    <div className="question relative">
      <div className="font-semibold text-[#003c2f] flex justify-between items-center min-w-[370px] md:min-w-[42rem] py-5">
        <h3>{title}</h3>{" "}
        {isOpen ? (
          <BsChevronUp
            className="cursor-pointer"
            size={24}
            onClick={() => {
              setIsOpen(!isOpen);
            }}
          />
        ) : (
          <BsChevronDown
            className="cursor-pointer"
            size={24}
            onClick={() => {
              setIsOpen(!isOpen);
            }}
          />
        )}
      </div>
      <div
        className={` ${
          isOpen ? "block" : "hidden"
        } py-5 max-w-2xl text-[#65816d] text-[16px] tracking-normal`}
      >
        {description}
      </div>
    </div>
  );
};

export default QuestionComponent;
