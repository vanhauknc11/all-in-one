import Image from "next/image";
import React from "react";
import TitleBonsai from "./TitleBonsai";
import QuestionComponent from "./Question/QuestionComponent";

const questionData = [
  {
    title: "How does the free trial work?",
    description:
      "When you start your trial with Bonsai you'll receive full, unlimited access to the plan you chose. You will need to enter your credit card information to begin your trial, but don't worry - we won't charge you anything until the trial ends. You can cancel at any time during the trial period via your Subscriptions settings.",
  },
  {
    title: "Can I change my plan anytime?",
    description:
      "Yes, you can upgrade, downgrade or cancel your plan at any time via your Subscription settings.",
  },
  {
    title: "Is my Bonsai subscription tax deductible?",
    description:
      "Yes, the IRS allows for tax deduction for software you use to run your business. Bonsai's expense tracking even automatically categorizes your subscription as a deductible software expense.",
  },
  {
    title: "What is your refund policy?",
    description:
      "If you contact us within two weeks of being charged for your subscription, we will be happy to issue a refund for you. Beyond those two weeks, you will need to cancel or modify the subscription from the Subscriptions tab in Settings to avoid future charges, but refunds will not be issued. This applies to both monthly and annual plans.",
  },
];

const QuestionContent = () => {
  return (
    <div className="flex relative my-16 px-2">
      <div className="content md:pl-48">
        <TitleBonsai title="Frequently asked questions" />
        <div className="flex flex-col items-start gap-5">
          {questionData.map((question, index) => (
            <QuestionComponent
              key={index}
              title={question.title}
              description={question.description}
            />
          ))}
        </div>
      </div>
      <div className="image-right hidden md:block">
        <Image
          src="/images/bonsai/branch.png"
          alt="branch"
          width={500}
          height={500}
        />
      </div>
    </div>
  );
};

export default QuestionContent;
