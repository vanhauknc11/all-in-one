import Image from "next/image";
import React from "react";
import { BsBox } from "react-icons/bs";
import { AiOutlineArrowRight } from "react-icons/ai";
const BonsaiSubmenu = () => {
  return (
    <div className="flex justify-between items-center py-6 text-gray-500 bg-[#FFF]">
      <div className="sub-menu">
        <h2 className="mb-5 font-bold text-black">Client Management</h2>
        <ul className="flex flex-col gap-3 text-sm">
          <li>CRM</li>
          <li>Scheduling</li>
          <li>Proposals</li>
          <li>Contracts</li>
          <li>Forms</li>
        </ul>
      </div>

      <div className="sub-menu">
        <h2 className="mb-5 font-bold text-black">Project Management</h2>
        <ul className="flex flex-col gap-3 text-sm">
          <li>Time tracking</li>
          <li>Tasks</li>
          <li>File sharing</li>
          <li>Client portal</li>
          <li>Collaboration</li>
        </ul>
      </div>

      <div className="sub-menu">
        <h2 className="mb-5 font-bold text-black">Financial Management</h2>
        <ul className="flex flex-col gap-3 text-sm">
          <li>Invoicing</li>
          <li>Payments</li>
          <li>Accounting</li>
          <li>Taxes</li>
          <li>Banking</li>
        </ul>
      </div>

      <div className="sub-menu self-start">
        <h2 className="mb-5 font-bold text-black ">{`What's new`}</h2>
        <ul className="flex flex-col gap-3 text-sm self">
          <li>
            <Image
              src="/images/bonsai/calendar.jpeg"
              alt="calendar"
              width={100}
              height={100}
            />
          </li>
          <li>Branded scheduling portal</li>
          <li className="text-[#22ad01] font-semibold underline">Learn more</li>
        </ul>
      </div>
    </div>
  );
};

export const BonsaiSubMenuSecond = () => {
  return (
    <div className="flex justify-between items-center py-6 text-gray-500 bg-[#FFF]">
      <div className="sub-menu">
        <h2 className="mb-5 font-bold text-black">Business Services</h2>
        <ul className="flex flex-col gap-3 text-sm">
          <li>Consulting</li>
          <li>Marketing</li>
          <li>Recruiting</li>
          <li>Virtual Assistant</li>
          <li>More...</li>
        </ul>
      </div>

      <div className="sub-menu">
        <h2 className="mb-5 font-bold text-black">Creative and Digital</h2>
        <ul className="flex flex-col gap-3 text-sm">
          <li>Design</li>
          <li>Photography</li>
          <li>Software Development</li>
          <li>Writing</li>
          <li>More...</li>
        </ul>
      </div>

      <div className="sub-menu">
        <h2 className="mb-5 font-bold text-black">Professional Services</h2>
        <ul className="flex flex-col gap-3 text-sm">
          <li>Coaching</li>
          <li>Architecture</li>
          <li>Accounting</li>
          <li>Legal</li>
          <li>More...</li>
        </ul>
      </div>

      <div className="sub-menu self-start">
        <h2 className="mb-5 font-bold text-black ">Other services</h2>
        <ul className="flex flex-col gap-3 text-sm">
          <li>Event Planning</li>
          <li>Interior Design</li>
          <li>Real-Estate</li>
          <li>More...</li>
          <li>More...</li>
        </ul>
      </div>
    </div>
  );
};

export const BonsaiSubMenuThird = () => {
  return (
    <div className="flex sub-menu py-5 bg-[#FFF]">
      <div className="border-r-[1px] border-gray-300">
        <ul className="flex flex-col text-sm text-black">
          <li className="p-3 text-[#22ad01] bg-green-100 rounded-md flex justify-between">
            <span>Contract Templates</span>
            <AiOutlineArrowRight size={22} />
          </li>
          <li className="p-3 min-w-[300px] ">Proposal Templates</li>
          <li className="p-3 min-w-[300px] ">Invoice Templates</li>
          <li className="p-3 min-w-[300px] ">Quote Templates</li>
          <li className="p-3 min-w-[300px] ">Scope of Work Templates</li>
          <li className="p-3 min-w-[300px] ">Brief Templates</li>
          <li className="p-3 min-w-[300px] ">Form Templates</li>
        </ul>
      </div>
      <div className="flex flex-col items-center gap-10">
        <p className="text-[10px] self-start text-gray-400 pl-10">
          Featured contract templates
        </p>
        <div className="flex justify-between items-center gap-10 px-10">
          <div>
            <ul className="flex flex-col gap-2 text-gray-500 text-sm">
              <li>Client Contract Template</li>
              <li>Free Design Contract Template</li>
              <li>Free Freelance Contract Template</li>
              <li>Free Hourly Rate Contract Template</li>
              <li>Free Non-Disclosure Agreement Template</li>
              <li>Free Project Management Agreement Template</li>
            </ul>
          </div>
          <div>
            <ul className="flex flex-col gap-2 text-gray-500 text-sm">
              <li>Client Contract Template</li>
              <li>Free Design Contract Template</li>
              <li>Free Freelance Contract Template</li>
              <li>Free Hourly Rate Contract Template</li>
              <li>Free Non-Disclosure Agreement Template</li>
              <li>Free Project Management Agreement Template</li>
            </ul>
          </div>
        </div>
        <p className="see-all">See all templates</p>
      </div>
    </div>
  );
};
export const BonsaiSubMenuFour = () => {
  return (
    <div className="flex justify-start gap-20 items-center py-6 text-gray-500 bg-[#FFF]">
      <div className="sub-menu self-start">
        <h2 className="mb-5 font-bold text-black">Learn</h2>
        <ul className="flex flex-col gap-3 text-sm">
          <li>Blog</li>
          <li>Help Center</li>
        </ul>
      </div>

      <div className="sub-menu">
        <h2 className="mb-5 font-bold text-black">Tools</h2>
        <ul className="flex flex-col gap-3 text-sm">
          <li>Contract Maker</li>
          <li>Self-Employed Taxes Hub</li>
          <li>Freelance Rates Explorer</li>
        </ul>
      </div>
    </div>
  );
};

export const NoMenuItem = () => {
  return (
    <div className="text-center text-black pt-5 flex flex-col items-center mt-5 gap-4 min-h-[250px]  bg-[#FFF]">
      <BsBox size={50} />
      <h2>No Resources </h2>
    </div>
  );
};

export default BonsaiSubmenu;
