import React from "react";

interface TitleBonsaiProp {
  title: string;
}
const TitleBonsai: React.FC<TitleBonsaiProp> = ({ title }) => {
  return (
    <h2 className="text-[#003c2f] text-center px-5 text-2xl md:text-[42px] font-semibold my-8">
      {title}
    </h2>
  );
};

export default TitleBonsai;
