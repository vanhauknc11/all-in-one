"use client";
import React from "react";
import TitleBonsai from "./TitleBonsai";
import Image from "next/image";
import useCheckMobileScreen from "../hooks/useCheckMobile";

const TrustedContent = () => {
  const isMobile = useCheckMobileScreen();
  return (
    <div className="mx-auto mt-20 mb-10">
      <TitleBonsai title="Trusted by 500,000+ business owners" />
      <div className="flex items-center justify-center mt-7">
        <div className="pr-3 md:pr-10 relative img-performance">
          <Image
            src="/images/bonsai/performance.svg"
            alt="performance"
            width={isMobile ? 150 : 280}
            height={150}
          />
        </div>

        <Image
          src="/images/bonsai/bestapp.svg"
          alt="bestapp"
          className="pl-2 md:pl-10"
          width={isMobile ? 150 : 300}
          height={150}
        />
      </div>
    </div>
  );
};

export default TrustedContent;
