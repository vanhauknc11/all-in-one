import Image from "next/image";
import React from "react";

const TutorialContent = () => {
  return (
    <div className="my-20 grid grid-cols-1 md:grid-cols-2 ">
      <div className="flex justify-end items-center">
        <Image
          src="/images/bonsai/tutorial.png"
          alt="lettalk"
          width={500}
          height={500}
        />
      </div>
      <div className="max-w-lg px-4 flex flex-col gap-10">
        <div className="flex flex-col gap-5">
          <h2 className="text-[#003c2f] text-[32px] font-semibold md:pr-14">
            Need help? We’ll be right with you
          </h2>
          <p className="text-[#65816d] text-[16px] tracking-normal">
            Our friendly, speedy, Customer Support has all the answers and is
            here to help you.
          </p>
          <p className="font-bold text-[#22ad01] underline cursor-pointer">
            Contact us
          </p>
        </div>
        <div className="flex flex-col gap-5">
          <h2 className="text-[#003c2f] text-[32px] font-semibold md:pr-14">
            Quick to set up Even easier to use
          </h2>
          <p className="text-[#65816d] text-[16px] tracking-normal">
            {`You get 7 days to try it for free. And if it's not what you
            expected, we guarantee your money back within the first two weeks.`}
          </p>
          <div className="bg-[#22ad01] px-4 py-3 rounded-full text-white cursor-pointer w-[150px] text-center">
            Get started
          </div>
        </div>
      </div>
    </div>
  );
};

export default TutorialContent;
