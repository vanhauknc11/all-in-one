import React from "react";
import BonsaiHeader from "./BonsaiHeader";
import "./bonsai.css";
import BonsaiMobileHeader from "./BonsaiMobileHeader";
import PlanContent from "./PlanContent";
import CardList from "./CardList";
import QuestionContent from "./QuestionContent";
import TrustedContent from "./TrustedContent";
import FeedbackContent from "./FeedbackContent";
import TutorialContent from "./TutorialContent";
const HelloBonsaiPage = () => {
  return (
    <div>
      <BonsaiHeader />
      <BonsaiMobileHeader />
      <PlanContent />
      <CardList />
      <QuestionContent />
      <TrustedContent />
      <FeedbackContent />
      <TutorialContent />
    </div>
  );
};

export default HelloBonsaiPage;
