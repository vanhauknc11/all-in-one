import "./globals.css";
import { Inter } from "next/font/google";

const nunito = Inter({ subsets: ["latin"] });

export const metadata = {
  title: "Vanhauknc | Make a new era",
  description: "Here is my site where i can deploy practice projects",
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <body className={nunito.className}>{children}</body>
    </html>
  );
}
