import React from "react";
import MessengerContainer from "./MessengerContainer";
import Image from "next/image";

const MessengerContent = () => {
  return (
    <div className="bg-white py-9">
      <MessengerContainer>
        <div className="grid grid-cols-1 md:grid-cols-2 ">
          <div className="left-content">
            <h2 className="messenger-title">Tụ họp mọi lúc, mọi nơi</h2>
            <h3 className="text-base text-gray-600">
              Với Messenger, việc kết nối với những người mình yêu mến thật đơn
              giản và thú vị.
            </h3>
            <div
              className="bg-[#0a7cff]
               text-white
                rounded-full
                 px-3 py-4 
                 my-5 
                 text-center
                  text-lg
                   hover:bg-[#4d4dff]
                    transition
                    cursor-pointer
            "
            >
              Tiếp tục dưới tên{" "}
              <span className="font-semibold">Nguyễn Hậu</span>
            </div>
            <a href="#" className="text-blue-600 cursor-pointer text underline">
              Chuyển tài khoản
            </a>

            <div className="flex my-5 gap-5">
              <Image
                src="/images/messenger/appstore.png"
                alt="app store"
                width={110}
                height={25}
              />
              <Image
                src="/images/messenger/microsoft.png"
                alt="microsoft"
                width={110}
                height={25}
              />
            </div>
          </div>
          <div className="right-content md:relative">
            <Image
              className="md:absolute top-0 right-0"
              src="/images/messenger/des-image.png"
              alt="des-image"
              width={600}
              height={500}
            />
          </div>
        </div>
      </MessengerContainer>
    </div>
  );
};

export default MessengerContent;
