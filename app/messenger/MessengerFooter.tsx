import React from "react";
import Container from "../components/Container";
import Image from "next/image";

const MessengerFooter = () => {
  return (
    <Container>
      <ul className="flex justify-between items-center text-center gap-3 footer ">
        <li className="font-semibold "> Meta 2023.</li>
        <li className="hidden sm:block">
          Logo của Apple và Google Play là nhãn hiệu hàng hóa thuộc chủ sở hữu
          tương ứng.
        </li>
        <li>Chính sách quyền riêng tư</li>
        <li>Chính sách cookie</li>
        <li className="hidden sm:block">Điều khoản</li>
        <li className="hidden sm:block">Tiếng việt</li>
        <li>
          from{" "}
          <Image
            src="/images/messenger/meta.png"
            alt="meta"
            width={100}
            height={100}
          />
        </li>
      </ul>
    </Container>
  );
};

export default MessengerFooter;
