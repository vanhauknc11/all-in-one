"use client";
import React from "react";
import MessengerContainer from "./MessengerContainer";
import Image from "next/image";
import { IoMenu, IoClose } from "react-icons/io5";
import MessengerItem from "./MessengerItem";
import MobileMenu from "./MobileMenu";

const MessengerHeader = () => {
  const [mobileMenu, setMobileMenu] = React.useState(false);
  return (
    <div className="bg-white py-8">
      <MessengerContainer>
        <div className="flex justify-between items-center ">
          <Image
            src="/images/messenger/logo.png"
            alt="logo"
            width={40}
            height={40}
          />
          <div className="flex items-center gap-6 font-semibold">
            <MessengerItem content="Tính năng" />
            <MessengerItem content="Ứng dụng dành cho máy tính" />
            <MessengerItem content="Quyền riêng tư & an toàn" />
            <MessengerItem content="Dành cho nhà phát triển" />

            <div
              className="block sm:hidden cursor-pointer"
              onClick={() => {
                setMobileMenu(!mobileMenu);
              }}
            >
              {mobileMenu ? <IoClose size={32} /> : <IoMenu size={32} />}
            </div>
          </div>
        </div>

        {mobileMenu && <MobileMenu />}
      </MessengerContainer>
    </div>
  );
};

export default MessengerHeader;
