import React from "react";

interface MessengerItemProp {
  content: string;
}

const MessengerItem: React.FC<MessengerItemProp> = ({ content }) => {
  return (
    <div className="messengerItem relative hidden md:block cursor-pointer ">
      {content}
    </div>
  );
};

export default MessengerItem;
