import React from "react";

const MobileMenu = () => {
  return (
    <div className="h-screen z-10 pt-10">
      <ul>
        <li className="p-5 mb-2 font-semibold bg-gray-100 text-gray-500 rounded-md ">
          Tính năng
        </li>
        <li className="p-5 mb-2 font-semibold bg-gray-100 text-gray-500 rounded-md ">
          Ứng dụng cho máy tính
        </li>
        <li className="p-5 mb-2 font-semibold bg-gray-100 text-gray-500 rounded-md ">
          Quyền riêng tư & an toàn
        </li>
        <li className="p-5 mb-2 font-semibold bg-gray-100 text-gray-500 rounded-md ">
          Dành cho nhà phát triển
        </li>
      </ul>
    </div>
  );
};

export default MobileMenu;
