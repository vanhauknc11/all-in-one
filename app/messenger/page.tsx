import React from "react";
import MessengerHeader from "./MessengerHeader";
import MessengerContent from "./MessengerContent";
import MessengerFooter from "./MessengerFooter";

const MessengerPage = () => {
  return (
    <div>
      <MessengerHeader />
      <MessengerContent />
      <MessengerFooter />
    </div>
  );
};

export default MessengerPage;
