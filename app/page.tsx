import MessengerContent from "./messenger/MessengerContent";
import MessengerFooter from "./messenger/MessengerFooter";
import MessengerHeader from "./messenger/MessengerHeader";

export default function Home() {
  return (
    <div>
      <MessengerHeader />
      <MessengerContent />
      <MessengerFooter />
    </div>
  );
}
